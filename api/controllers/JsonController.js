/**
 * JsonController
 *
 * @description :: Server-side logic for managing Jsons
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	updateBotConfig: function (req, res) {
        async.waterfall([
            function (next) {
                var obj = req.body ? req.body : sails.services.config;
                next(null, obj);
            },
            function (body, next) {
                var path = req.user && req.user.config.path ? req.user.config.path : '/root/.freqtrade/config.json';
                next(null, path, body);
            },
            function (path, data, next) {
                require('jsonfile').writeFile(path, data, function (err) {
                    next(err, path, data);
                })
            },
            function (path, data) {
                res.json({path: path, data: req.body});
            }
        ], function (err) {
            res.send(err);
        });
    },
    getBotConfig: function (req, res) {
        async.waterfall([
            function (next) {
                require('fs').readFile('/root/.freqtrade/config.json', function (err, data) {
                    if (err) res.send(err);
                    else next(null, data);
                });
            },
            function (data, next) {
                res.send(data);
            }
        ], function (err) {
            res.send(err);
        })
    }
};

