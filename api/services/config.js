module.exports = {
    "max_open_trades": 5,
    "stake_currency": "BTC",
    "stake_amount": 0.2,
    "fiat_display_currency": "USD",
    "dry_run": true,
    "unfilledtimeout": 600,
    "bid_strategy": {
        "ask_last_balance": 0.0
    },
    "exchange": {
        "name": "bittrex",
        "key": "a86dfc05348c48a18495262395c2fd44",
        "secret": "e81cabfc79b248d2a093d1c56faa444e",
        "pair_whitelist": [
            "BTC_ZEC",
            "BTC_XLM",
            "BTC_POWR",
            "BTC_ADA",
            "BTC_XMR"
        ],
        "pair_blacklist": [
            "BTC_DOGE",
            "BTC_RDD"
        ]
    },
    "experimental": {
        "use_sell_signal": true,
        "sell_profit_only": false
    },
    "telegram": {
        "enabled": true,
        "token": "501726753:AAHyBvqIUA8Wkxrw0URIFq4qk9h3gpOXZz4",
        "chat_id": "508183585"
    },
    "initial_state": "running",
    "internals": {
        "process_throttle_secs": 5
    }
};

