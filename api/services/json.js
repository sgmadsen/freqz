module.exports = {
    validate: function (str) {
        var data;
        try {
            data = JSON.parse(str);
            next(null, data);
        } catch (e) {
            next(e);
        }

        next(null, data);
    },
    toJSON: function (str, next) {
        var data = JSON.parse(str);
        if(data) next(null, data);
        else next("Invalid JSON")
    }
};